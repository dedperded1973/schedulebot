package application.com.services.interfaces;

import application.com.models.Lesson;

import java.io.IOException;

public interface NotificationSender
{
    void send(Lesson lesson) throws IOException;
}
