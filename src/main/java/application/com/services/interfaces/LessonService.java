package application.com.services.interfaces;

import application.com.models.Lesson;

import java.sql.SQLException;
import java.util.List;

public interface LessonService
{
    List<Lesson> getByToday() throws SQLException;
}
