package application.com.services;

import application.com.models.Lesson;
import application.com.services.interfaces.NotificationSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class NotificationSenderImpl implements NotificationSender
{
    @Value("${telegram.url}")
    private String url;

    @Value("${bot.token}")
    private String token;

    private final RestTemplate rest = new RestTemplate();

    @Override
    public void send(Lesson lesson)
    {
        String uri = url.formatted(token, lesson.getGroupID(), buildMessage(lesson));

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);

        rest.exchange(uri, HttpMethod.GET, entity, String.class);
    }

    private String buildMessage(Lesson lesson)
    {
        String message = "Напоминаю:\nСегодня в <b>%s</b> занятие <b>%s</b>.\nПреподаватель - <b>%s</b>. \nКабинет <b>%s</b> корпуса <b>%s</b>.";

        return message.formatted
        (
            (lesson.getStartedAt().getHour() + ":" + lesson.getStartedAt().getMinute()),
            lesson.getSubject().getName(),
            lesson.getSubject().getTeacher(),
            lesson.getCabinet(),
            lesson.getBlock()
        );
    }
}
