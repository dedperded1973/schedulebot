package application.com.services;

import application.com.databases.DatabaseConnection;
import application.com.databases.queries.GetTodayLessonsQuery;
import application.com.models.Lesson;
import application.com.services.interfaces.LessonService;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

@Service
public class LessonServiceImpl implements LessonService
{
    private final DatabaseConnection connection;

    public LessonServiceImpl(DatabaseConnection connection)
    {
        this.connection = connection;
    }

    @Override
    public List<Lesson> getByToday() throws SQLException
    {
        return GetTodayLessonsQuery.query(connection.statement(), LocalDate.now().getDayOfMonth());
    }
}
