package application.com.configurations;

import application.com.quartz.factory.AutowiringSpringBeanJobFactory;
import lombok.extern.log4j.Log4j2;
import org.quartz.*;
import org.quartz.impl.calendar.BaseCalendar;
import org.quartz.spi.OperableTrigger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.*;

import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

@Configuration
@ConditionalOnProperty(name = "quartz.enabled")
public class QuartzConfiguration
{
    private final ApplicationContext applicationContext;

    public QuartzConfiguration(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory()
    {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);

        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(Trigger... triggers)
    {
        SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();

        Properties properties = new Properties();

        properties.setProperty("org.quartz.scheduler.instanceName", "NewsPublisher");
        properties.setProperty("org.quartz.scheduler.instanceId", "ID");

        schedulerFactory.setOverwriteExistingJobs(true);
        schedulerFactory.setAutoStartup(true);
        schedulerFactory.setQuartzProperties(properties);
        schedulerFactory.setJobFactory(springBeanJobFactory());
        schedulerFactory.setWaitForJobsToCompleteOnShutdown(true);

        if (triggers.length != 0) {
            schedulerFactory.setTriggers(triggers);
        }

        return schedulerFactory;
    }

    public static CronTriggerFactoryBean createCronTrigger(JobDetail detail, String cronExpression, String name) throws ParseException
    {
        CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();

        factoryBean.setJobDetail(detail);
        factoryBean.setCronExpression(cronExpression);
        factoryBean.setStartDelay(0L);
        factoryBean.setName(name);
        factoryBean.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING);
        factoryBean.afterPropertiesSet();

        return factoryBean;
    }

    public static JobDetailFactoryBean createJobDetail(Class<? extends Job> jobClass, String name, JobDataMap map)
    {
        JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();

        factoryBean.setName(name);
        factoryBean.setJobClass(jobClass);
        factoryBean.setDurability(true);
        factoryBean.setJobDataMap(map);
        factoryBean.afterPropertiesSet();

        return factoryBean;
    }
}
