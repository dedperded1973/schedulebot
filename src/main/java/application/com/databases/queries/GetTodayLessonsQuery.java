package application.com.databases.queries;

import application.com.models.Block;
import application.com.models.Lesson;
import application.com.models.Subject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GetTodayLessonsQuery
{
    private static final String QUERY = """
       SELECT
                lessons.id as id,
                subject_id,
                cabinet,
                block,
                started_at,
                name,
                teacher,
                group_id
       FROM db.lessons
       JOIN db.subjects subject on subject.id = subject_id
       WHERE EXTRACT('Day' FROM started_at) = %o;
    """.trim();

    public static List<Lesson> query(Statement statement, Integer day) throws SQLException
    {
        List<Lesson> lessons = new ArrayList<Lesson>();

        ResultSet set = statement.executeQuery(QUERY.formatted(day));

        while (set.next())
        {
            Lesson lesson = toLesson(set, toSubject(set));

            lessons.add(lesson);
        }

        return lessons;
    }

    private static Subject toSubject(ResultSet set) throws SQLException
    {
        return Subject
                    .builder()
                    .id(set.getInt("subject_id"))
                    .name(set.getString("name"))
                    .teacher(set.getString("teacher"))
                    .build();
    }

    private static Lesson toLesson(ResultSet set, Subject subject) throws SQLException
    {
        return Lesson
                    .builder()
                    .subject(subject)
                    .id(set.getInt("id"))
                    .groupID(set.getString("group_id"))
                    .cabinet(set.getInt("cabinet"))
                    .startedAt(set.getTimestamp("started_at").toLocalDateTime())
                    .block(Block.valueOf(set.getString("block")))
                    .build();
    }
}
