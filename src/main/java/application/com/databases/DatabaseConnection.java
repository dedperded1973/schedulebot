package application.com.databases;

import application.com.configurations.DatabaseConfiguration;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class DatabaseConnection
{
    private final Connection connection;

    private final DatabaseConfiguration configuration;

    public DatabaseConnection(DatabaseConfiguration configuration) throws SQLException
    {
        this.configuration = configuration;
        this.connection = connection();
    }

    public Connection connection() throws SQLException
    {
        return DriverManager.getConnection
        (
                configuration.getUrl(),
                configuration.getUsername(),
                configuration.getPassword()
        );
    }

    public Statement statement() throws SQLException
    {
        return connection.createStatement();
    }
}
