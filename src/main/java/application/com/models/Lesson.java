package application.com.models;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Lesson
{
    private Integer id;

    private Integer cabinet;
    private Block block;

    private Subject subject;

    private String groupID;

    private LocalDateTime startedAt;
}
