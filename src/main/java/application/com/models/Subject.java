package application.com.models;

import lombok.*;

@Builder
@Setter @Getter
@NoArgsConstructor
@AllArgsConstructor
public class Subject
{
    private Integer id;

    private String name;
    private String teacher;
}
