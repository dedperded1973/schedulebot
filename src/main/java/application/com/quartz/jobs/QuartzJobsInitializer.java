package application.com.quartz.jobs;

import application.com.configurations.QuartzConfiguration;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

import java.text.ParseException;

@Configuration
public class QuartzJobsInitializer
{
    @Bean("LoadLessonsJob")
    public JobDetailFactoryBean job()
    {
        return QuartzConfiguration.createJobDetail(LoadLessonsJob.class, "LoadLessonsJob", new JobDataMap());
    }

    @Bean
    public CronTriggerFactoryBean trigger(@Qualifier("LoadLessonsJob") JobDetail detail) throws ParseException
    {
        return QuartzConfiguration.createCronTrigger(detail, "* 0 0 ? * 1-6 *", "LoadLessonsJob");
    }
}
