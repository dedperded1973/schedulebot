package application.com.quartz.jobs;

import application.com.configurations.QuartzConfiguration;
import application.com.models.Lesson;
import application.com.services.interfaces.LessonService;
import application.com.services.interfaces.NotificationSender;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


@Slf4j
@Component
@DisallowConcurrentExecution
public class LoadLessonsJob implements Job
{
    @Value("${job.notificationBefore}")
    private Integer notificationBefore;

    private final LessonService service;
    private final NotificationSender sender;


    public LoadLessonsJob(LessonService service, NotificationSender sender) throws SchedulerException
    {
        this.service = service;
        this.sender = sender;
    }

    @Override
    public void execute(JobExecutionContext context)
    {
        try
        {
            Scheduler scheduler = context.getScheduler();
            scheduler.unscheduleJob(context.getTrigger().getKey());

            List<Lesson> lessons = service.getByToday();

            JobDataMap map = new JobDataMap();
            map.put("sender", sender);

            lessons.forEach(it ->
            {
                try
                {
                    map.put("lesson", it);

                    JobDetailFactoryBean detailFactoryBean = QuartzConfiguration.createJobDetail(SendNotificationJob.class, "NotificationJob_" + UUID.randomUUID(), map);
                    JobDetail detail = detailFactoryBean.getObject();

                    CronTriggerFactoryBean triggerFactoryBean = QuartzConfiguration.createCronTrigger(detail, expression(it.getStartedAt()), "LoadLessonsJob");
                    CronTrigger trigger = triggerFactoryBean.getObject();

                    scheduler.scheduleJob(detail, trigger);
                    scheduler.start();
                }
                catch (ParseException | SchedulerException exception)
                {
                    log.error
                    (
                            "Job ** {} ** failed @ {}, error: {}",
                            context.getJobDetail().getKey().getName(),
                            context.getFireTime(),
                            exception.getMessage()
                    );
                }
            });
        }
        catch (SQLException | SchedulerException exception)
        {
            log.error
            (
                    "Loading lessons failed @ {}, error: {}",
                    context.getFireTime(),
                    exception.getMessage()
            );
        }
    }

    private String expression(LocalDateTime startedAt)
    {
        startedAt = startedAt.minusHours(notificationBefore);
        return "* %s %s ? * 1-6 *".formatted(startedAt.getMinute(), startedAt.getHour() == 0 ? 1 : startedAt.getHour());
    }
}
