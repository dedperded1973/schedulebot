package application.com.quartz.jobs;

import application.com.models.Lesson;
import application.com.services.interfaces.NotificationSender;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Slf4j
@Component
@DisallowConcurrentExecution
public class SendNotificationJob implements Job
{
    @Override
    public void execute(JobExecutionContext context)
    {
        try
        {
            Scheduler scheduler = context.getScheduler();
            scheduler.unscheduleJob(context.getTrigger().getKey());

            JobDetail detail = context.getJobDetail();
            JobDataMap map = detail.getJobDataMap();

            Lesson lesson = (Lesson) map.get("lesson");

            NotificationSender sender = (NotificationSender) map.get("sender");
            sender.send(lesson);
        }
        catch (IOException | SchedulerException exception)
        {
            log.error
            (
                    "Job ** {} ** failed @ {}, error: {}",
                    context.getJobDetail().getKey().getName(),
                    context.getFireTime(),
                    exception.getMessage()
            );
        }
    }
}


