FROM openjdk:17

WORKDIR /application
COPY . /application/.

RUN ./mvnw clean
RUN ./mvnw package -DskipTests

ARG JAR_FILE=./target/*.jar
COPY ${JAR_FILE} application.jar

ENTRYPOINT ["java", "-jar", "application.jar"]



